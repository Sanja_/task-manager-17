package ru.karamyshev.taskmanager.command.data.base64;

import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.constant.DataConstant;
import ru.karamyshev.taskmanager.dto.Domain;
import ru.karamyshev.taskmanager.enumerated.Role;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

public class DataBase64LoadCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-dtbnld";
    }

    @Override
    public String name() {
        return "data-base64-load";
    }

    @Override
    public String description() {
        return "Save data to base64 file.";
    }

    @Override
    public void execute() throws IOException, ClassNotFoundException {
        System.out.println("[DATA BASE64 LOAD]");

        final String dataBase64 = new String(Files.readAllBytes(Paths.get(DataConstant.FILE_BASE64)));
        final byte[] data = Base64.getDecoder().decode(dataBase64);

        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(data);
        final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        final Domain domain = (Domain) objectInputStream.readObject();

        serviceLocator.getProjectService().load(domain.getProjects());
        serviceLocator.getTaskService().load(domain.getTasks());
        serviceLocator.getUserService().load(domain.getUsers());
        objectInputStream.close();
        System.out.println("[OK]");

    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}
