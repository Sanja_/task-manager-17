package ru.karamyshev.taskmanager.command.data.base64;

import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.constant.DataConstant;
import ru.karamyshev.taskmanager.enumerated.Role;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;

public class DataBase64ClearCommand extends AbstractCommand implements Serializable {

    @Override
    public String arg() {
        return "-dtbsclr";
    }

    @Override
    public String name() {
        return "data-base64-clear";
    }

    @Override
    public String description() {
        return "Remove base64 file.";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("[REMOVE BASE64 FILE]");
        final File file = new File(DataConstant.FILE_BASE64);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}
