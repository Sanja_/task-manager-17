package ru.karamyshev.taskmanager.command.user;

import ru.karamyshev.taskmanager.api.service.IAuthService;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.util.TerminalUtil;

public class UserRenameLoginCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "rnm-lgn";
    }

    @Override
    public String name() {
        return "rename-login";
    }

    @Override
    public String description() {
        return "Rename login account";
    }

    @Override
    public void execute() {
        IAuthService authService = serviceLocator.getAuthService();
        String userId = authService.getUserId();
        String currentLogin = authService.getCurrentLogin();
        System.out.println("CHANGE ACCOUNT LOGIN");
        System.out.println("CURRENT LOGIN: " + currentLogin);
        System.out.println("[ENTER NEW LOGIN]");
        String newLogin = TerminalUtil.nextLine();
        authService.renameLogin(userId, currentLogin, newLogin);
        System.out.println("[OK]");
    }

}
