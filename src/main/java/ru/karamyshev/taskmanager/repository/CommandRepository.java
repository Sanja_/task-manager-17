package ru.karamyshev.taskmanager.repository;

import ru.karamyshev.taskmanager.api.repository.ICommandRepository;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.command.data.base64.DataBase64ClearCommand;
import ru.karamyshev.taskmanager.command.data.base64.DataBase64LoadCommand;
import ru.karamyshev.taskmanager.command.data.base64.DataBase64SaveCommand;
import ru.karamyshev.taskmanager.command.data.binary.DataBinaryClearCommand;
import ru.karamyshev.taskmanager.command.data.binary.DataBinaryLoadCommand;
import ru.karamyshev.taskmanager.command.data.binary.DataBinarySaveCommand;
import ru.karamyshev.taskmanager.command.info.*;
import ru.karamyshev.taskmanager.command.project.*;
import ru.karamyshev.taskmanager.command.system.ExitCommand;
import ru.karamyshev.taskmanager.command.system.UserLoginCommand;
import ru.karamyshev.taskmanager.command.system.UserLogoutCommand;
import ru.karamyshev.taskmanager.command.system.UserRegistryCommand;
import ru.karamyshev.taskmanager.command.task.*;
import ru.karamyshev.taskmanager.command.user.*;

import java.util.ArrayList;
import java.util.List;

public class CommandRepository implements ICommandRepository {

    private static final Class[] COMMANDS = new Class[]{
            HelpCommand.class, SystemInfoCommand.class, VersionShowCommand.class, AboutCommand.class,
            CommandsShowCommand.class, ArgumentsShowCommand.class,

            UserRegistryCommand.class, ExitCommand.class, UserLogoutCommand.class, UserLoginCommand.class,

            UserRenameLoginCommand.class, UserRenamePasswordCommand.class, ProfileShowCommand.class,

            TasksShowCommand.class, TaskClearCommand.class, TasksCreateCommand.class, TaskShowByIdCommand.class,
            TaskShowByNameCommand.class, TaskUpdateByIdCommand.class, TaskUpdateByIndexCommand.class, TaskRemoveByIdCommand.class,
            TaskRemoveByIndexCommand.class, TaskRemoveByNameCommand.class, TaskShowByIndexCommand.class,

            ProjectShowCommand.class, ProjectClearCommand.class, ProjectCreateCommand.class, ProjectShowByIdCommand.class,
            ProjectShowByNameCommand.class, ProjectUpdateByIdCommand.class, ProjectUpdateByIndexCommand.class, ProjectRemoveByIdCommand.class,
            ProjectRemoveByIndexCommand.class, ProjectRemoveByNameCommand.class, ProjectShowByIndexCommand.class,

            UserLockCommand.class, UserUnlockCommand.class, UserRemoveCommand.class,

            DataBinaryClearCommand.class, DataBinaryLoadCommand.class, DataBinarySaveCommand.class,

            DataBase64ClearCommand.class, DataBase64LoadCommand.class, DataBase64SaveCommand.class

    };

    final private List<AbstractCommand> commands = new ArrayList<>();

    {
        for (final Class aClass : COMMANDS) {
            try {
                final Object commandInstance = aClass.newInstance();
                final AbstractCommand command = (AbstractCommand) commandInstance;
                commands.add(command);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    private final List<String> COMMANDS_LIST = getCommands(commands);

    private final List<String> ARGS = getArgs(commands);

    public List<String> getCommands(final List<AbstractCommand> values) {
        if (values == null || values.size() == 0) return null;
        final List<String> commandsList = new ArrayList<>();
        for (int i = 0; i < values.size(); i++) {
            final String command = values.get(i).name();
            if (command == null || command.isEmpty()) continue;
            commandsList.add(command);
        }
        return commandsList;
    }

    public List<String> getArgs(final List<AbstractCommand> values) {
        if (values == null || values.size() == 0) return null;
        final List<String> argsList = new ArrayList<>();
        for (int i = 0; i < values.size(); i++) {
            final String arg = values.get(i).arg();
            if (arg == null || arg.isEmpty()) continue;
            argsList.add(arg);
        }
        return argsList;
    }

    public List<AbstractCommand> getTerminalCommands() {
        return commands;
    }

    public List<String> getCommands() {
        return COMMANDS_LIST;
    }

    public List<String> getArgs() {
        return ARGS;
    }

}
